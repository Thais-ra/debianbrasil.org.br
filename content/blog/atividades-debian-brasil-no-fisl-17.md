---
title: "Atividades Debian Brasil no FISL 17"
kind: article
created_at: 2016-07-08 19:58
author: Giovani Ferreira
---

A comunidade Debian Brasil está com uma série de atividades programadas para o
FISL 17.

A programação está disponível na Wiki do projeto Debian, que pode ser acessada
[aqui.](https://wiki.debian.org/Brasil/Eventos/EncontroComunitarioFISL2016)

Nos vemos no FISL!
