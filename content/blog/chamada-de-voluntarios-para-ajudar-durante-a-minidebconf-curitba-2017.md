---
title: "Chamada de voluntários para ajudar durante a MiniDebConf Curitba 2017"
kind: article
created_at: 2017-03-06 13:29
author: Paulo Henrique de Lima Santana
---

Estamos com uma chamada de voluntários aberta para quem quiser ajudar durante os
dias da MiniDebConf Curitiba 2017.

Tem várias tarefas que precisaremos de ajuda, então basta preencher o formulário.