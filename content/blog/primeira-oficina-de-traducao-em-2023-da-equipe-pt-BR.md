---
title: "Primeira oficina de tradução em 2023 da equipe pt_BR"
kind: article
created_at: 2023-04-01 10:00
author: Thiago Pezzo
---

The Brazilian translation team [debian-l10n-portuguese](https://lists.debian.org/debian-l10n-portuguese/)
realizou [a primeira oficina de 2023](https://wiki.debian.org/Brasil/Traduzir/Campanhas/OficinaDDTP2023)
em fevereiro, com ótimos resultados:

 * A oficina foi direcionada para iniciantes, usando o [DDTP/DDTSS](https://ddtp.debian.org/ddtss).
 * Dois dias de oficina mão-na-massa via Jitsi.
 * Nos dias seguintes, continuidade dos trabalho de tradução de forma
independente, com o suporte da equipe.
 * Quantidade de pessoas inscritos(as): 29
 * Novos(as) contribuidores(as) no DDPT/DDTSS: 22
 * Traduções dos(as) novos(as) participantes: 175
 * Revisões dos(as) novos(as) participantes : 261

Nosso foco era completar as descrições dos 500 pacotes mais populares (popcon).
Apesar de não termos conseguido chegar à 100% do ciclo de tradução, grande parte
dessas descrições estão em andamento e com um pouco mais de trabalho estarão
disponíveis à comunidade.

Agradeço aos(às) participantes pelas contribuições. As oficinas foram bem
movimentadas e, muito além das traduções em si, conversamos sobre diversas faces
da comunidade Debian. Esperamos ter ajudado aos(às) iniciantes a contribuir com o
projeto de maneira frequente.

Agradeço em especial ao Charles (charles) que ministrou um dos dias da oficina,
ao Paulo (phls) que sempre está aí dando uma força, e ao Fred Maranhão pelo seu
incansável trabalho no DDTSS.

[Equipe de tradução do português do Brasil](https://wiki.debian.org/Brasil/Traduzir)
