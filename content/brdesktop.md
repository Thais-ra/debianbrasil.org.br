---
title: "BrDesktop"
kind: page
created_at: 2020-09-13 20:09
author: Paulo Henrique de Lima Santana
---

# BrDesktop

<span style="color:red">**Status: este projeto foi descontinuado**</span>

BrDesktop, anteriormente conhecido como Debian-BR-CDD, era um sistema Debian
GNU/Linux desenvolvido com foco nos(as) usuários(as) brasileiros(as). Veja as
características que fariam você utilizar o BrDesktop no seu Desktop ;)

- Simples de instalar e usar
- Traduzido para o idioma português
- Você podia usar sem instalar
- Softwares era selecionados pelos próprios usuários
- Seguro e estável
- Oferecia softwares atualizados
- Bonito
- Oficialmente Debian!

Site: <https://brdesktop.org/brdesktop>

### Mais informações

We were an official package for the Debian GNU/Linux operating system that has
been designed specifically for those of you who live, work and use Linux as your
desktop operating system in Brazil.

Due to this package being incorporated into the Debain official package, we no
longer need to create or update this any longer.

BrDesktop is a preconfigured packaged distribution for brazilian Debian
GNU/Linux home desktop users in pure blend. The main goal of BrDesktop is to
allow brazilian users an easy way to install and use Debain right from the start
without needing to supplement additional Debain packages after the initial
installation.

Additional packages include portuguese language desktop as well as additional
brazilian localization options and packages pre-selected.

The bundle is meant to include what most brazilian users would come to want and
expect in a desktop application.

The BrDesktop Lenny iso image uses the Debian GNU/Linux 5.0.1 Lenny packages
up-to-date as of 23-April-2016.

### Arquivos do projeto Debian-BD-CDD

<http://valessiobrito.com.br/projetos/debian/debian-br-cdd>